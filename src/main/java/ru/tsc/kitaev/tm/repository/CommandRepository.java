package ru.tsc.kitaev.tm.repository;

import ru.tsc.kitaev.tm.constant.ArgumentConst;
import ru.tsc.kitaev.tm.constant.TerminalConst;
import ru.tsc.kitaev.tm.model.Command;

public class CommandRepository {

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Display system information..."
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Display developer info..."
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Display list of commands..."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Display program version..."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application..."
    );

    public static final Command[] COMMANDS = new Command[] {
            INFO, ABOUT, HELP, VERSION, EXIT
    };

    public static Command[] getCommands() {
        return COMMANDS;
    }

}
